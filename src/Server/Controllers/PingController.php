<?php

namespace OcServer\Controllers;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PingController
{
    public function pingAction(Request $request): JsonResponse
    {
        return new JsonResponse(['pong' => time()]);
    }
}
