<?php

namespace OcServer;

use OcServer\Controllers\PingController;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Router
{
    /**
     * Get all routes
     *
     * @return RouteCollection All routes
     */
    public static function getRoutes(): RouteCollection
    {
        $routeDefinitions = self::getRouteDefinitions();
        $rootRoutes = new RouteCollection();

        foreach ($routeDefinitions as $routeDefinition) {
            $route = new Route(
                $routeDefinition['path'],
                // http://symfony.com/doc/current/routing.html#special-routing-parameters
                ['_controller' => $routeDefinition['controller']],
                $routeDefinition['requirements'] ?? [], // requirements
                [], // options
                '', // host
                ['http', 'https'], // schemas
                [$routeDefinition['httpMethod']]
            );
            $rootRoutes->add($routeDefinition['name'], $route);
        }

        return $rootRoutes;
    }

    /**
     * Get route definitions
     *
     * @return string[] Route definitions
     */
    protected static function getRouteDefinitions(): array
    {
        $routeDefinitions = [
            // auth
            [
                'name' => 'ping',
                'path' => '/api/v1/ping',
                'controller' => PingController::class . '::pingAction',
                'httpMethod' => 'GET',
            ],
        ];

        return $routeDefinitions;
    }
}
